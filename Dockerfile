FROM gradle:4.7.0-jdk8-alpine AS build

USER root
RUN apk update && apk upgrade && \
    apk add --no-cache bash git nss openssl \
    && rm -rf /var/cache/apk/*
WORKDIR /home/gradle
COPY . .
RUN if [ ! -f ./gradlew ]; then \
            gradle \
                --quiet \
                --no-daemon \
                --no-build-cache \
                wrapper; \
    fi
RUN ./gradlew \
            --quiet \
            --no-daemon \
            --no-build-cache \
            build \
            -x test

FROM openjdk:8-jre-slim AS production
ENV BUILD_DIR="/home/gradle/build" \
    APP_DIR="/opt/backend" \
    APP_NAME="backend.jar"
RUN groupadd -r backend && useradd -r -g backend backend
USER backend
WORKDIR /opt/backend
COPY --from=build --chown=backend:backend $BUILD_DIR/libs/* ${APP_DIR}/${APP_NAME}
COPY --from=build --chown=backend:backend $BUILD_DIR/resources/main/application.properties ${APP_DIR}

EXPOSE 8080

ENTRYPOINT [ "java", "-jar", "backend.jar", "--spring.config.location=file:./"  ]
